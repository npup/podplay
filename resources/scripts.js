window.addEventListener('load', function () {
  if ('Notification' in window) {
    var grantNotificationsPermission = function () {
      if (window.Notification.permission !== "granted") {
        window.Notification.requestPermission();
      }
    }();
  }

  if (show = localStorage.getItem('currentShow')) {
    switchShow(show);
  }

  var settingsCheckboxes = document.querySelectorAll(".savedSetting");
  settingsCheckboxes.forEach(function(element){
    var key =element.id;
    if (localStorage.getItem(key) == "true") {
      element.checked = true;
      element.dispatchEvent(new Event('change'));
    }
  });

  initProgressBar();
},false);

var isPlaying = false;

(function updatePlayedTime() {
  setInterval(
    function() {
      if (isPlaying) {
        var hide = false;
        if (player.duration-60 <= player.currentTime) {
          hide = true;
        }

        updatePlayedTimeForEpisode(
          window.flags.currentUser,
          window.flags.currentEpisode,
          player.currentTime,
          hide
        );
      }
    }, 15000);

  /**
   * Check every second if the audio is playing,
   *   set playbutton state accordingly.
   */
  setInterval(
    function() {
      var player   = document.getElementById('player');
      var button   = document.getElementById('play-btn');
      var autoPlay = document.getElementById("autoPlay").checked;
      if (player.paused !== false) {
        isPlaying = false;
        button.className = '';
        if (player.duration <= Math.ceil(player.currentTime) && autoPlay) {
          playNext();
        }
      } else {
        isPlaying = true;
        button.className = 'pause';
      }
    },
    1000
  );
})();

function playNext() {
  var button      = document.getElementById('play-btn');
  var nowPlaying  = window.flags.currentEpisode;
  var nextEpisode = document.querySelector('.episodes li[data-id="'+ nowPlaying +'"] + li');
  var showId      = window.flags.currentShow;
  var show        = document.querySelector('.show[data-show-id="'+ showId +'"]');

  if (nextEpisode) {
    nextEpisode.click();
    button.click();

    sendNotification(
      show.getAttribute('data-show-title'),
      nextEpisode.getAttribute('data-title'),
      show.getAttribute('data-show-image')
    );
  }
}

function saveState(el) {
    var state = el.checked;
    localStorage.setItem(el.id, state);
}

function switchShow(showId) {
  window.flags.currentShow = showId;
  localStorage.setItem('currentShow', showId);
  loadEpisodes();
}

function loadEpisodes() {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function () {
    window.episodeList = JSON.parse(xhr.response);
    window.episodeList.sort(function (a,b) {
      var ad = new Date(a['pubDate']);
      var bd = new Date(b['pubDate']);
      if (bd == ad) return 0;
      if (bd > ad) return -1;
      if (bd < ad) return +1;
    });
    renderEpisodes(episodeList);
    autoSelectEpisode();
  };

  xhr.send('action=getEpisodes&'+
    'show_id=' + window.flags.currentShow +
    '&user_id='+ window.flags.currentUser +
    '&hidden=' + document.getElementById("displayHidden").checked
  );
}

function updatePlayedTimeForEpisode(userId, episodeId, playedTime, hide) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send('action=updatePlayedTime'+
    '&episode_id=' + episodeId +
    '&user_id='    + userId +
    '&played_time='+ playedTime +
    '&hidden='     + hide
  );
}

function autoSelectEpisode() {
  var currentEpisode;
  var episode;
  if (currentEpisode = localStorage.getItem('currentEpisode')) {
    var episodeData = window.episodeList;
    if (!window.episodeList) {

      return;
    }

    episodeData.forEach(function(ep) {
      if (typeof ep !== "undefined") {
        if (ep._podplayer_id == currentEpisode) {
          episode = ep;
        }
      }
    });
    if (!episode) {

      return;
    }

    playFile(
      episode['enclosure']['@attributes']['url'],
      episode['enclosure']['@attributes']['type'],
      episode['title'],
      episode['_podplayer_id']
    );

    if (episode.playedTime != null && isPlaying !== true) {
      player.currentTime = episode.playedTime;
    }
  }
}

function renderEpisodes(episodeList) {
  var ul = document.querySelector('.episodes > ul');
  var li;
  while (ul.firstChild) {
    ul.removeChild(ul.firstChild);
  }
  if (episodeList.length <= 0) {

    return;
  }
  episodeList.forEach(function(episode) {
    var rd = new Date(episode['pubDate']);
    li = document.createElement('li');
    li.onclick = function () {
        playFile(episode['enclosure']['@attributes']['url'],
          episode['enclosure']['@attributes']['type'],
          episode['title'],
          episode['_podplayer_id']);
    };
    li.setAttribute('data-id',    episode['_podplayer_id']);
    li.setAttribute('data-title', episode['title']);
    li.setAttribute('data-playedTime', parseFloat(episode['playedTime']));
    if (parseFloat(episode['playedTime']) > 0) {
      li.className += " initiated";
    }
    if (episode['hidden'] === "true") {
      li.className += " hidden";
    }
    li.innerText =  rd.toLocaleString('sv-SE').substr(0,10) +' '+ episode['title'];
    ul.appendChild(li);
  });
  var episodeCount = document.querySelector(
    '.show[data-show-id="'+ window.flags.currentShow +'"] .episode-count'
  );
  episodeCount.innerText = episodeList.length;
}

function playFile(uri, type, title, id) {
  if (window.flags.currentEpisode == id) {

    return;
  }

  if (isPlaying == true) {
    if (!confirm('Are you sure?')) {
      return;
    }
  }

  isPlaying = false;
  var episode;
  var episodeData = window.episodeList;
  for (i = 0; i <= episodeData.length; i++) {
    if (episodeData[i]._podplayer_id == id) {
      episode = episodeData[i];
      break;
    }
  }

  localStorage.setItem('currentEpisode', episode._podplayer_id);
  window.flags.currentEpisode = episode._podplayer_id;

  document.title = 'PodPlay - ' + title;
  document.getElementById('current-track').innerText = title;
  document.getElementById('current-description').innerHTML = episode.description;

  var player = document.getElementById('player');
  if (player) {
    player.pause();
    document.getElementById('play-btn').removeEventListener('click',function () {
      togglePlay()
    });
    player.parentNode.removeChild(player);
  }

  player = document.createElement("audio");
  if (player.canPlayType(type)) {
    player.setAttribute("id", 'player');
    player.setAttribute("src", uri);
    player.setAttribute("ontimeupdate", "initProgressBar()");
    document.getElementById('player-container').appendChild(player);
  }

  player.load();
  if (episode.playedTime != null) {
    player.currentTime = episode.playedTime;
  }
  initProgressBar();
  setMetaData(episode, player);
}

function setMetaData(episode, player) {
  var showId = window.flags.currentShow;
  var show   = document.querySelector('.show[data-show-id="'+ showId +'"]');
  if ("mediaSession" in navigator) {
    var skipTime = 10;
    navigator.mediaSession.setActionHandler('seekbackward', function() {
      player.currentTime = Math.max(player.currentTime - skipTime, 0);
    });
    navigator.mediaSession.setActionHandler('seekforward', function() {
      player.currentTime = Math.min(player.currentTime + skipTime, player.duration);
    });
    navigator.mediaSession.setActionHandler('play', function() {
      player.play();
    });
    navigator.mediaSession.setActionHandler('pause', function() {
      player.pause();
    });
    navigator.mediaSession.metadata = new MediaMetadata({
      title: episode.title,
      artist: show.getAttribute('data-show-title'),
      album: show.getAttribute('data-show-title'),
      artwork: [{ src: show.getAttribute('data-show-image') }]
    });
  }
}

function initProgressBar() {
  var player       = document.getElementById('player');
  var length       = isNaN(player.duration) ? 0 : player.duration;
  var current_time = player.currentTime;

  // calculate total length of value
  var totalLength = calculateTotalValue(length)
  document.getElementById("end-time").innerHTML = totalLength;

  // calculate current value time
  var currentTime   = calculateCurrentValue(current_time);
  document.getElementById("start-time").innerHTML = currentTime;
  var progressbar   = document.getElementById('seek-obj');
  if (current_time <= 0 || length <= 0) {
    progressbar.value = 0;
  } else {
    progressbar.value = (current_time / length);
  }
  progressbar.addEventListener("click", seek);

  if (player.currentTime === player.duration) {
    document.getElementById('play-btn').className = "";
  }

  function seek(event) {
    var percent = event.offsetX / this.offsetWidth;
    player.currentTime = percent * player.duration;
    progressbar.value = percent / 100;
  }
}

function playToggle() {
    var player = document.getElementById('player');
    var button = document.getElementById('play-btn');
    if (player.paused !== false) {
      player.play();
      button.className = 'pause';
    } else {
      button.className = '';
      player.pause();
    }
}

function calculateTotalValue(length) {
  var hours   = Math.floor(length / 3600);
  var minutes = Math.floor((length - (hours*3600)) / 60),
    seconds_int = length - minutes * 60,
    seconds_str = seconds_int.toString(),
    seconds = seconds_str.substr(0, 2);

    if (isNaN(hours))   { hours = 0;   }
    if (isNaN(minutes)) { minutes = 0; }
    if (isNaN(seconds)) { seconds = 0; }

    minutes = (minutes < 10) ? "0"+ minutes : minutes;
    seconds = (seconds < 10) ? "0"+ seconds : seconds;
     
  return hours + ':' + minutes + ':' + seconds
}

function calculateCurrentValue(currentTime) {
  var current_hour = parseInt(currentTime / 3600) % 24,
    current_minute = parseInt(currentTime / 60) % 60,
    current_seconds_long = currentTime % 60,
    current_seconds = current_seconds_long.toFixed();

  return current_hour +
        ':' + 
        (current_minute < 10 ? "0" + current_minute : current_minute) +
        ":" + 
        (current_seconds < 10 ? "0" + current_seconds : current_seconds);
}

function addShow() {
  var newUrl = prompt("Provide URL");
  if (newUrl != null) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'ajaxLoader.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
      top.location = '/';
    };
    xhr.send('action=addShow&show_id='+ newUrl + '&user_id='+ window.flags.currentUser);
  }
}

function removeShow(userId, showId) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'ajaxLoader.php');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send('action=removeShow&show_id='+ showId + '&user_id='+ userId);
}

function toggleManageMode() {
  if (!window.flags.manageMode) {
    var shows = document.querySelectorAll('section.show-wrapper > .show > a');
    shows.forEach(function(show) {
      addControls(show, [
        {
          name: "refresh",
          action: (function() {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'ajaxLoader.php');
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function() {
              top.location = '/';
            };
            xhr.send('action=refresh&user_id='+ window.flags.currentUser);
            event.stopPropagation();
            event.preventDefault();
          })
        },
        {
          name: "delete",
          action: (function() {
            if (confirm('Are you sure?')) {
              removeShow(
                window.flags.currentUser,
                show.parentNode.getAttribute('data-show-id')
              );
              show.parentNode.removeChild(show);
              event.stopPropagation();
              event.preventDefault();
            }
          })
        }
      ]);
    });

    var episodes = document.querySelectorAll('section.episodes > ul > li');
    episodes.forEach(function(episode) {
      addControls(
        episode, [
          {
            name: "hide",
            action: (function () {
              updatePlayedTimeForEpisode(
                window.flags.currentUser,
                episode.getAttribute('data-id'),
                1,
                true
              );
              episode.parentNode.removeChild(episode);
              event.stopPropagation();
              event.preventDefault();
            })
          },
          {
            name: "reset",
            action: (function () {
              updatePlayedTimeForEpisode(
                window.flags.currentUser,
                episode.getAttribute('data-id'),
                0,
                false
              );
              event.stopPropagation();
              event.preventDefault();
            })
          },
        ]
      );
    });

    function addControls(element, list) {
      list.forEach(function(button) {
        var child = document.createElement('div');
        child.className  = 'manage-controls '+ button.name;
        child.innerText  = button.name;
        child.onclick    = button.action;
        element.appendChild(child);
      });
    }
    window.flags.manageMode = true;
  } else {
    var manageControls = document.querySelectorAll('.manage-controls');
    manageControls.forEach(function(control) {
      control.parentNode.removeChild(control);
    });
    window.flags.manageMode = false;
  }

  document.querySelectorAll('section.show-wrapper > .show').forEach(function(show){
    show.setAttribute('data-show-manage', window.flags.manageMode.toString());
  });
}

function sendNotification(showTitle, episodeTitle, icon) {
  if ('Notification' in window) {
    var notification = new Notification(
      episodeTitle, {
        icon: icon,
        body: 'Automatically changed episode to next episode (' + episodeTitle + ') on show ' + showTitle,
      }
    );
    notification.onclick = function () {
      notification.close();
    };
  }
}
