<?php
include('loader.php');
$subscription = new subscription();
$action = $_REQUEST['action'];
$user   = $_REQUEST['user_id'];

switch ($action) {
    case 'getEpisodes':
        $show       = $_REQUEST['show_id'];
        $showHidden = ($_REQUEST['hidden'] === "true") ? true: false;
        echo json_encode($subscription->getEpisodes($user, $show, $showHidden));
    break;
    case 'addShow':
        $show   = $_REQUEST['show_id'];
        $feeds = $subscription->getFeedList($user);
        $feeds[] = $show;
        $subscription->updateSubscriptions($user,$feeds);
        $subscription->updateEpisodes($user);
    break;
    case 'updatePlayedTime':
        $episode    = $_REQUEST['episode_id'];
        $playedTime = $_REQUEST['played_time'];
        $hide       = $_REQUEST['hidden'];

        $subscription->updatePlayTime($user, $episode, $playedTime, $hide);
    break;
    case 'removeShow':
        $show   = $_REQUEST['show_id'];
        $subscription->removeShow($user, $show);
    break;
    case 'refresh':
        $subscription->updateEpisodes($user);
    break;
}
