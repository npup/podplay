<?php
include('loader.php');
$id_token  = $_POST['token'];
$CLIENT_ID = '785839189864-de3rre6bf50iqb0amahms7pp9vtjp24p.apps.googleusercontent.com';
$client    = new Google_Client(['client_id' => $CLIENT_ID]);
$payload   = $client->verifyIdToken($id_token);
if ($payload) {
  echo $payload['sub'];
  $_SESSION['googleToken'] = (string)$payload['sub'];
} else {
  echo 'failed';
}
