<?php

class subscription
{
    protected $requestBuilder;

    public function __construct()
    {
        ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_WARNING);
        $this->requestBuilder = new podplay\api\RequestBuilder();
    }

    public function updateSubscriptions($userId, $subscriptions)
    {
        $showData = $subscriptions;
        $requestPayload = '';
        foreach ($showData as $subscription) {
            $rssURI    = file_get_contents($subscription);
            $rssObject = simplexml_load_string(
                $rssURI,
                'SimpleXMLElement',
                LIBXML_NOCDATA
            );
            if (!$rssObject) {

                continue;
            }

            $payload = json_decode(
                json_encode(
                    $rssObject
                ), JSON_OBJECT_AS_ARRAY
            );

            unset($payload['channel']['item']);
            $payload['channel']['rssFeed']    = $subscription;
            $payload['channel']['id']         = sha1($subscription);
            $payload['channel']['user']['id'] = $userId;
            $payload                          = array_map('array_filter', $payload);
            $indexUpdate = [
                "update" => [
                    "_index" => "podplay_show",
                    "_type" => "show",
                    "_id" => sha1($subscription) . "_UID_" . $userId,
                ]
            ];
            $requestPayload .= json_encode($indexUpdate, JSON_FORCE_OBJECT) . "\n";
            $requestPayload .= json_encode(
                    [
                        'doc' => $payload,
                        'doc_as_upsert' => true
                    ]
                    , JSON_FORCE_OBJECT) . "\n";
        }

        $this->updateEpisodes($userId);

        return $this->requestBuilder->makeRequest(
            '_bulk',
            'show',
            'podplay_show',
            [],
            'post',
            'application/x-ndjson',
            $requestPayload
        );
    }

    public function removeShow($user, $show)
    {
        return $this->requestBuilder->makeRequest(
            $show . "_UID_" . $user,
            'show/',
            'podplay_show',
            [],
            'delete',
            '',
            ''
        );
    }

    public function updatePlayTime($user, $episode, $playedTime, $hide = false)
    {
        $payload = [
            "doc" => [
                'playedTime' => $playedTime,
                'hidden' => $hide
            ]
        ];

        return $this->requestBuilder->makeRequest(
            '_update',
            'episode/' . $episode . "_UID_" . $user,
            'podplay',
            [],
            'post',
            'application/json',
            json_encode($payload, JSON_FORCE_OBJECT)
        );
    }

    public function updateEpisodes($userId)
    {
        $subscriptions  = $this->getFeedList($userId);
        $requestPayload = '';
        foreach ($subscriptions as $index => $subscription) {
            $rssURI = file_get_contents($subscription);
            $rssObject = simplexml_load_string(
                $rssURI,
                'SimpleXMLElement',
                LIBXML_NOCDATA
            );
            if (!$rssObject) {
                continue;
            }

            $showData = json_decode(
                json_encode(
                    $rssObject
                ), JSON_OBJECT_AS_ARRAY
            );

            $episodes = $showData['channel']['item'];
            foreach ($episodes as $episode) {
                $episode['_podplayer_id'] = sha1($episode['guid']);
                $episode['_show_id']      = sha1($subscription);
                $episode['user']['id']    = $userId;
                $indexUpdate = [
                    "update" => [
                        "_index" => "podplay",
                        "_type"  => "episode",
                        "_id"    => $episode['_podplayer_id'] . "_UID_" . $userId,
                    ]
                ];
                $requestPayload .= json_encode($indexUpdate, JSON_FORCE_OBJECT) . "\n";
                $requestPayload .= json_encode(
                        [
                            'doc' => $episode,
                            'doc_as_upsert' => true
                        ]
                        , JSON_FORCE_OBJECT) . "\n";
            }
        }

        if (!empty($requestPayload)) {
            return $this->requestBuilder->makeRequest(
                '_bulk',
                'episode',
                'podplay',
                [],
                'post',
                'application/x-ndjson',
                $requestPayload
            );
        }
    }

    public function getFeedList($userId)
    {
        $shows = $this->getSubscriptions($userId);
        $feed = [];
        foreach ($shows as $show) {
            $feed[] = $show['channel']['rssFeed'];
        }

        return $feed;
    }

    public function getSubscriptions($userId)
    {
        $feed = [];
        $query = [
            "query" => [
                "term" => [
                    "channel.user.id.keyword" => [
                        "value" => (string)$userId
                    ]
                ]
            ],
            "from" => 0,
            "size" => 500
        ];
        $query = json_encode($query, JSON_FORCE_OBJECT);
        $showData = $this->requestBuilder->makeRequest(
            '_search',
            'show',
            'podplay_show',
            [],
            'GET',
            'application/json',
            $query
        );

        $showData = json_decode($showData, JSON_OBJECT_AS_ARRAY);
        $shows = $showData['hits']['hits'];
        foreach ($shows as $show) {
            $feed[] = $show['_source'];
        }

        return $feed;
    }

    public function getEpisodes($userId, $showId, $displayHidden = false)
    {
        $episodes = [];
        $query = [
            "query" => [
                "bool" => [
                    "must" => [
                        [
                            "term" => [
                                "user.id.keyword" => [
                                    "value" => (string)$userId
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "_show_id.keyword" => [
                                    "value" => (string)$showId
                                ]
                            ]
                        ],
                    ]
                ]
            ],
            "from" => 0,
            "size" => 1500
        ];

        if (!$displayHidden) {
            $query['query']["bool"]["must_not"] = [
                [
                    "term" => [
                        "hidden.keyword" => [
                            "value" => "true"
                        ]
                    ]
                ]
            ];
        }

        $query = json_encode($query);

        $episodeData = $this->requestBuilder->makeRequest(
            '_search',
            'episode',
            'podplay',
            [],
            'GET',
            'application/json',
            $query
        );

        $episodeData = json_decode($episodeData, JSON_OBJECT_AS_ARRAY);
        $extractEpisodes = $episodeData['hits']['hits'];
        foreach ($extractEpisodes as $episode) {
            $episodes[] = $episode['_source'];
        }

        return $episodes;
    }

    public function reindex()
    {
        $users = [];
        $query = [
            "query" => [
                "match_all" => []
            ]
        ];
        $query = json_encode($query, JSON_FORCE_OBJECT);
        $showData = $this->requestBuilder->makeRequest(
            '_search',
            'show',
            'podplay_show',
            [],
            'GET',
            'application/json',
            $query
        );

        $showData = json_decode($showData, JSON_OBJECT_AS_ARRAY);
        $shows = $showData['hits']['hits'];
        foreach ($shows as $show) {
            $users[$show['_source']['channel']['user']['id']] = $show['_source']['channel']['user']['id'];
        }
        foreach ($users as $user) {
            $this->updateEpisodes($user);
        }
    }
}
