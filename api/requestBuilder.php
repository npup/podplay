<?php

namespace podplay\api;

class RequestBuilder {
    const ELASTIC_PROTOCOL = 'http';
    const ELASTIC_HOST     = '127.0.0.1';
    const ELASTIC_PORT     = 9200;

    /**
     * @param        $method
     * @param null   $documentType
     * @param null   $index
     * @param array  $queryParams
     * @param string $requestMethod
     * @param null   $contentType
     * @param null   $payload
     *
     * @return bool
     * @throws \Exception
     */
    public function makeRequest(
        $method,
        $documentType  = null,
        $index         = null,
        $queryParams   = [],
        $requestMethod = 'GET',
        $contentType   = null,
        $payload       = null
    ) {
        $url      = self::ELASTIC_PROTOCOL . '://'. self::ELASTIC_HOST.':'.self::ELASTIC_PORT;
        $urlParts = $this->_joinUrlPath([$index, $documentType, $method]);
        $urlParts .= sizeof($queryParams)
            ? '?' . http_build_query($queryParams)
            : '';

        $requestURL = $this->_joinUrlPath([ $url, $urlParts ]);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $requestURL);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($requestMethod));
        if('HEAD' == $requestMethod) {
            curl_setopt($curl, CURLOPT_NOBODY, 1);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($payload) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: {$contentType}", 'Content-Length: ' . strlen($payload)]);
        }
        $result = curl_exec($curl);
        $alive  = curl_error($curl);
        $info   = curl_getinfo($curl);
        $info   = array_merge($info,['request_method' => $requestMethod]);
        $errors = $this->hasErrors(
            $info,
            $alive,
            $result,
            $payload
        );
        curl_close($curl);

        if($errors instanceof \Exception) {

            throw $errors;
        }

        if (empty($alive) && !$errors) {

            return $result;
        }

        throw new \Exception($alive);
    }

    /**
     * Join URL path parts with a "/" betwwen parts.
     *
     * @param array $parts
     *
     * @return string
     */
    protected function _joinUrlPath(array $parts)
    {
        $cleanParts = [];
        foreach ($parts as $p) {
            $cleanParts[] = trim($p, "/");
        }

        return implode("/", array_filter($cleanParts));
    }

    protected function hasErrors(
        $requestInfo,
        $lastError,
        $result,
        $payload)
    {
        $error = json_decode($result, JSON_OBJECT_AS_ARRAY);
        if (404 == $requestInfo['http_code'] && 'HEAD' == $requestInfo['request_method']) {

            return new \Exception('[{"type": "index_not_found_exception"}]');
        }

        if (isset($error['error']['root_cause']) && is_array($error['error']['root_cause'])) {
            $errorsAsString = json_encode($error['error']['root_cause'], JSON_PRETTY_PRINT);

            return new \Exception($errorsAsString);
        }

        if (isset($error['errors']) && $error['errors'] == true) {
            if (isset($error["items"]) && !empty($error["items"])) {
                $errItems = [];
                foreach ($error["items"] as $idx => $item) {
                    if (isset($item["index"]["error"]) && !empty($item["index"]["error"])) {
                        $errItems[] = implode(" | ", [
                            $item["index"]["error"]["type"],
                            $item["index"]["error"]["reason"],
                            "ID: " . $item["index"]["_id"]
                        ]);
                    }
                }
                $errorsAsString = implode("\n", $errItems);
            } else {
                $errorsAsString = json_encode($error, JSON_PRETTY_PRINT);
            }

            return new \Exception($errorsAsString);
        }

        return false;
    }
}
