<?php
include('loader.php');
?><!DOCTYPE>
<html>
<head>
    <title>PodPlay</title>
    <link rel="apple-touch-icon" sizes="180x180" href="media/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="media/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="media/favicon-16x16.png">
    <link rel="manifest" href="media/site.webmanifest">
    <meta charset="utf-8"/>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8;'/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="google-signin-client_id"
          content="785839189864-de3rre6bf50iqb0amahms7pp9vtjp24p.apps.googleusercontent.com">
    <script src="//apis.google.com/js/platform.js"></script>
    <script>
      var currentUser;
      function onSuccess(googleUser) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'validateToken.php');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
          currentUser = xhr.responseText;
          currentUser = window.localStorage.setItem(
            'token',
            currentUser
          );
          top.location = '/';
        };
        xhr.send('token=' + googleUser.getAuthResponse().id_token);
      }
      function onFailure(){
      }
    </script>

    <style type="text/css">
        @import url(//fonts.googleapis.com/css?family=Open+Sans);

        * {
            font-family: "Open Sans", verdana, arial, sans-serif;
            color: #DEDEDE;
        }

        body {
            background-color: #1a1a1a;
            overflow-x: hidden;
            margin: 0;
        }

        .wrapper {
            margin: 0 auto;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="g-signin2"
         data-theme="dark"
         data-longtitle="false"
         data-onsuccess="onSuccess"
         data-onfailure="onFailure"
         data-width="250"
         data-height="50"
    ></div>
    </div>
</body>
</html>
